from itertools import starmap
from typing import NamedTuple, Generator, Tuple, Union

import numpy as np

from model import GameState, Player, Ball, Vector2dFloat, Vector1dFloat, Vector2dInt


class Inputs(NamedTuple):
    left_player: Vector2dInt = np.zeros(2)  # [UP_PRESSED, DOWN_PRESSED]
    right_player: Vector2dInt = np.zeros(2)


FORCE_FACTOR = 0.01 * np.array((-1, 1))
FRICTION_FACTOR = 0.2
BUMP_FACTOR = 0.5
PADDLE_FACTOR = 0.1


def from_margin_coords(vector: Union[Vector1dFloat, Vector2dFloat], margin: Union[Vector1dFloat, Vector2dFloat]):
    return vector * (1 - margin)


def to_margin_coords(vector: Union[Vector1dFloat, Vector2dFloat], margin: Union[Vector1dFloat, Vector2dFloat]):
    return vector / (1 - margin)


def non_zero(vector: Union[Vector1dFloat, Vector2dFloat]):
    return np.sign(np.abs(vector))


def is_zero(vector: Union[Vector1dFloat, Vector2dFloat]):
    return 1 - non_zero(vector)


def conditional(cond, then_val, else_val):
    return cond * then_val + (1 - cond) * else_val


def conditional_negative(cond, value):
    return value - 2 * cond * value


def conditional_add(cond, base_value, amount):
    return base_value + cond * amount


def will_hit_x(position_ball: Vector2dFloat, velocity_ball: Vector2dFloat, ball_size: Vector1dFloat):
    position_ball_x = position_ball @ np.array((1, 0))
    position_ball_y = position_ball @ np.array((0, 1))
    velocity_ball_x = velocity_ball @ np.array((1, 0))
    velocity_vall_y = velocity_ball @ np.array((0, 1))

    factor = (np.array((ball_size - 1, 1 - ball_size)) - position_ball_x) / velocity_ball_x
    will_hit = is_zero(np.floor(factor))
    relevant_factor = factor @ will_hit
    hit_at = position_ball_y + relevant_factor * velocity_vall_y

    return will_hit, hit_at


def will_hit_pad(
        position_ball_y: Vector1dFloat,
        size_ball: Vector1dFloat,
        size_pad: Vector1dFloat,
        position_pad: Vector1dFloat,
):
    ball_bot_top = position_ball_y + np.array((1, -1)) * size_ball
    pad_top_bot = position_pad + np.array((-1, 1)) * size_pad

    has_hit = is_zero(np.sign(ball_bot_top - pad_top_bot).sum())
    return has_hit


def rectify(vector: Union[Vector1dFloat, Vector2dFloat], margin: Vector1dFloat):
    vector = to_margin_coords(vector, margin)

    pu_coordinates = vector.astype(np.int)
    direction = np.sign(vector)

    parity = pu_coordinates % 2
    in_range = vector % (np.sign(direction * 2 + 1))
    # in_range = vector % np.sign(vector)
    rectified = conditional(parity, direction - in_range, in_range)
    rectified = from_margin_coords(rectified, margin)

    return rectified, parity, pu_coordinates


def update_player(player: Player, inputs: Vector2dInt):
    new_velocity = player.velocity + inputs @ FORCE_FACTOR
    new_velocity -= FRICTION_FACTOR * new_velocity

    new_position = player.position + new_velocity

    new_position, parity, pu = rectify(new_position, player.size)

    bumps = np.abs(pu)

    new_velocity = conditional_negative(parity, new_velocity)
    new_velocity *= BUMP_FACTOR ** bumps

    return Player(new_position, new_velocity, player.size)


def update_ball(players: Tuple[Player, Player], ball: Ball):
    new_position = ball.position + ball.velocity
    new_position, *_ = rectify(new_position, ball.radius)

    new_velocity = ball.velocity

    will_hit, hit_at = will_hit_x(new_position, new_velocity, ball.radius)

    # TODO: reformat model
    pad_sizes = np.array((players[0].size, players[1].size))
    pad_velocities = np.array((players[0].velocity, players[1].velocity))
    pad_positions = np.array((players[0].position, players[1].position))

    has_hit = will_hit_pad(
        hit_at,
        ball.radius,
        pad_sizes @ will_hit,
        pad_positions @ will_hit
    )

    # new_velocity += has_hit * (pad_velocities @ will_hit) * PADDLE_FACTOR * np.array((0, 1))
    paddle_boosts = pad_velocities * PADDLE_FACTOR * np.array((0, 1))
    new_velocity = conditional_add(has_hit, new_velocity, will_hit @ paddle_boosts)
    new_velocity = np.clip(new_velocity, -1, 1)

    new_position = ball.position + new_velocity
    new_position, parity, _ = rectify(new_position, ball.radius)

    new_velocity = conditional_negative(parity, new_velocity)

    winner = (1 - has_hit) * np.array((1, -1)) @ will_hit

    return Ball(
        new_position,
        new_velocity,
        ball.radius,
    ), winner


def simulate(initial_game_state: GameState = GameState()) -> Generator[GameState, Tuple[GameState, Inputs], GameState]:
    game_state = initial_game_state

    while not game_state.winner:
        game_state, inputs = yield game_state
        *players, ball, _ = game_state

        new_players = starmap(update_player, zip(players, inputs))

        new_ball, winner = update_ball(players, ball)
        game_state = GameState(
            *new_players,
            new_ball,
            winner
        )

    yield game_state
