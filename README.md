# DeepQLearningPong

Look ma, no control flow statements! Intended to be rewritten in TensorFlow (instead of NumPy) for maximal speed with Deep Q-Learning.
This means
* No `if`-statements
* No loops (`for` or `while`)
* No libraries except `NumPy` / `TensorFlow`
* No `list`s, `dict`s or other `object`s; only `NumPy` arrays
* No direct array-indexing; have to use matrix multiplication with sparse vectors
* Additionally I opted not to use any of `==`, `!=`, etc. although I'm not sure that actually results in a speedup

This was quite the challenge, but as you can see, it works!