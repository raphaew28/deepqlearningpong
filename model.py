from enum import IntEnum
from typing import NamedTuple

import numpy as np

Vector1dFloat = np.ndarray
Vector2dFloat = np.ndarray
Vector1dInt = np.ndarray
Vector2dInt = np.ndarray


class Players(IntEnum):
    LEFT_PLAYER = -1
    NO_PLAYER = 0
    RIGHT_PLAYER = 1


class Player(NamedTuple):
    position: Vector1dFloat = 0
    velocity: Vector1dFloat = 0
    size: Vector1dFloat = 0.1


class Ball(NamedTuple):
    position: Vector2dFloat = np.zeros(2)
    velocity: Vector2dFloat = np.zeros(2)
    radius: Vector1dFloat = 0.02


class GameState(NamedTuple):
    playerLeft: Player = Player()
    playerRight: Player = Player()
    ball: Ball = Ball()
    winner: Players = Players.NO_PLAYER
