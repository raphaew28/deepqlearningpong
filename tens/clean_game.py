from typing import Generator, Tuple, Any, NamedTuple, List
import time
import sys

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt

from model import Players
from tens.learning import CompressedGameState
from collections import deque

from tqdm import tqdm


class Memory(NamedTuple):
    before: List[CompressedGameState]
    action: int
    reward: int
    after: List[CompressedGameState]


class SameLinePrinter:
    def __init__(self):
        self.last_line_length = 0

    def __call__(self, message):
        sys.stdout.write(chr(8) * self.last_line_length + message)
        sys.stdout.flush()
        self.last_line_length = len(message)

    def end(self, message):
        sys.stdout.write(chr(8) * self.last_line_length + message + '\n')
        sys.stdout.flush()
        self.last_line_length = 0


class PerformaceTimer:
    def __init__(self, window_size, message='Average Time: {average_time}s'):
        self.window_size = window_size
        self.message = message

        self.last_measurements = deque(maxlen=window_size)
        self.sum = 0

        self.start_time = 0
        self.average_time = 0

        self.printer = SameLinePrinter()

    def __enter__(self):
        self.start_time = time.perf_counter()

    def __exit__(self, *args):
        time_taken = time.perf_counter() - self.start_time
        self.sum += time_taken

        full = len(self.last_measurements) == self.last_measurements.maxlen
        if full:
            removed_measurement = self.last_measurements.popleft()
            self.sum -= removed_measurement

        self.last_measurements.append(time_taken)

        self.average_time = self.sum / len(self.last_measurements)

    def show(self, message=None):
        if message is None:
            message = self.message

        self.printer(message.format(self.average_time))


class print_tensors():
    def __init__(self, result, *inputs, **kwargs):
        self.result = result
        self.inputs = inputs
        self.kwargs = kwargs
        self.manager = tf.control_dependencies((tf.print(*inputs, **kwargs),))

    def __enter__(self):
        self.manager.__enter__()
        return tf.identity(self.result)

    def __exit__(self, *args):
        self.manager.__exit__(*args)


class Counter:
    def __init__(self, *amounts, restart=False, transform=None):
        self.amounts = amounts
        self.restart = restart
        self.transform = transform

    def __iter__(self):
        counters = list(self.amounts)

        def inner():
            while True:
                result = [False] * len(counters)

                for index in range(len(counters) - 1, -1, -1):
                    counters[index] -= 1
                    if counters[index] == 0:
                        result[index] = True
                        counters[index] = self.amounts[index]
                    else:
                        break
                else:
                    if not self.restart:
                        yield self._get_result(result)
                        break

                yield self._get_result(result)

        return inner()

    def __mul__(self, other):
        if not isinstance(other, Counter):
            raise TypeError(f"unsupported operand type(s) for *: '{type(self)}' and '{type(other)}'")
        return Counter(*self.amounts, *other.amounts, restart=self.restart and other.restart)

    def _get_result(self, result):
        if self.transform is None:
            if len(result) == 1:
                return result[0]
            else:
                return result
        else:
            return self.transform(result)


class InterpolateLinear:
    def __init__(self, start, end, steps, transform=lambda x: x):
        self.start = start
        self.end = end
        self.num_steps = steps

        self.step = (start - end) / steps
        self.value = start

        self.transform = transform

    def __getitem__(self, item):
        if item >= self.num_steps:
            return self.transform(self.end)
        return self.transform(self.start - item * self.step)

    def __iter__(self):
        value = self.start
        transform = self.transform

        def inner():
            nonlocal value
            while value > self.end:
                yield transform(value)
                value -= self.step
            while True:
                yield transform(self.end)

        return inner()

    def __next__(self):
        if self.value > self.end:
            value = self.value
            self.value -= self.step
            return self.transform(value)
        return self.transform(self.end)

    def _transformed(self, transform):
        def new_transform(x, base_transform=self.transform):
            return transform(base_transform(x))

        return InterpolateLinear(self.start, self.end, self.num_steps, new_transform)

    def __sub__(self, other):
        return self._transformed(lambda x: x - other)

    def __rsub__(self, other):
        return self._transformed(lambda x: other - x)

    def __add__(self, other):
        return self._transformed(lambda x: x + other)

    def __radd__(self, other):
        return self._transformed(lambda x: x + other)

    def __mul__(self, other):
        return self._transformed(lambda x: x * other)

    def __rmul__(self, other):
        return self._transformed(lambda x: x * other)

    def __truediv__(self, other):
        return self._transformed(lambda x: x / other)

    def __rtruediv__(self, other):
        return self._transformed(lambda x: other / x)

    def __floordiv__(self, other):
        return self._transformed(lambda x: x // other)

    def __rfloordiv__(self, other):
        return self._transformed(lambda x: other // x)

    def __float__(self):
        return self._transformed(float)

    def __abs__(self):
        return self._transformed(abs)

    def __bool__(self):
        return self._transformed(bool)

    def __complex__(self):
        return self._transformed(complex)

    def __int__(self):
        return self._transformed(int)


class Pong:
    def __init__(
            self,
            ball_size,
            paddle_sizes,

            image_size,
            margins,
            paddle_pixel_widths,

            paddle_factor,
            force_factor,
            friction_factor,
            bump_factor,

            history_length=4,
            action_repeat=4,
            replay_memory_size=10000,
            minibatch_size=32,
            replay_start_size=100,
            update_frequency=4,
            target_update_frequency=500,
            lagged_update_frequency=300,
            discount_factor=0.99,
            learning_rate=0.001,
            epsilons=None
    ):
        self.graph = tf.Graph()
        self.session = tf.Session(graph=self.graph)

        self.history = np.zeros((*image_size, history_length), np.float32)
        self.running_memory = deque(maxlen=history_length)
        self.action_repeat = action_repeat
        self.replay_memory = deque(maxlen=replay_memory_size)
        self.minibatch_size = minibatch_size
        self.discount_factor = discount_factor
        self.learning_rate = learning_rate
        self.epsilons = epsilons if epsilons is not None else InterpolateLinear(1, 0.1, 2000)

        self.online_update_counter = iter(Counter(update_frequency, restart=True))
        self.replay_start_counter = Counter(replay_start_size)
        self.target_update_counter = iter(Counter(target_update_frequency, restart=True))
        self.lagged_update_counter = iter(Counter(lagged_update_frequency, restart=True))

        with self.graph.as_default():
            self.ball_size = tf.constant(ball_size, tf.float32)
            self.paddle_sizes = tf.constant(paddle_sizes, tf.float32)

            self.image_size_value = image_size
            self.image_size = tf.constant(image_size, tf.float32)
            self.margins = tf.constant(margins, tf.float32)
            self.paddle_pixel_widths = tf.constant(paddle_pixel_widths, tf.float32)

            self.paddle_factor = tf.constant(paddle_factor, tf.float32)
            self.force_factor = tf.constant(force_factor, tf.float32)
            self.friction_factor = tf.constant(friction_factor, tf.float32)
            self.bump_factor = tf.constant(bump_factor, tf.float32)

            self.border = tf.constant((ball_size[0] - 1, 1 - ball_size[0]), tf.float32)

            self.image = tf.get_variable('image', np.prod(image_size), initializer=tf.zeros_initializer)

            self._make_ball()
            self._make_paddles()
            self._update_paddles()
            self._update_ball()
            self._draw()

            with tf.control_dependencies([self.step_paddles, self.step_ball]):
                self.winner = self._select((1 - self.has_hit) * tf.constant((1, -1), tf.float32), self.will_hit)

            self._make_q_networks()

            self.initializer = tf.global_variables_initializer()

            self.saver = tf.train.Saver()

        self.file_writer = tf.summary.FileWriter(f'summaries/{time.time()}', self.graph)

    def initialize(
            self,
            initial_inputs_value=None,
            initial_ball_velocity_value=None,
            initial_ball_position_value=None,
            initial_paddle_velocities_value=None,
            initial_paddle_positions_value=None,
    ):

        self.session.run(self.initializer)

        self._new_episode(
            initial_inputs_value,
            initial_ball_velocity_value,
            initial_ball_position_value,
            initial_paddle_velocities_value,
            initial_paddle_positions_value,
        )

    def _new_episode(
            self,
            initial_inputs_value=None,
            initial_ball_velocity_value=None,
            initial_ball_position_value=None,
            initial_paddle_velocities_value=None,
            initial_paddle_positions_value=None,
    ):
        self.session.run((
            self.set_inputs,
            self.set_ball_position,
            self.set_ball_velocity,
            self.set_paddle_positions,
            self.set_paddle_velocities,
        ), feed_dict={
            self.inputs_placeholder: initial_inputs_value or ((0, 0), (0, 0)),
            self.ball_velocity_placeholder: initial_ball_velocity_value or (
                np.random.rand() * -0.08,
                (np.random.rand() * 2 - 1) * 0.08,

                # -0.01,
                # 0.01555,
            ),
            self.ball_position_placeholder: initial_ball_position_value or (0, 0),
            self.paddle_velocities_placeholder: initial_paddle_velocities_value or (0, 0),
            self.paddle_positions_placeholder: initial_paddle_positions_value or (0, 0),
        })

        self._fill_initial_running_memory()
        self.history[:] = np.swapaxes(np.swapaxes([self.get_pic()] * self.history.shape[2], 0, 1), 1, 2)

    def advance(self, inputs=None):
        if inputs is not None:
            self.session.run(self.set_inputs, feed_dict={
                self.inputs_placeholder: inputs,
            })

        return self.session.run(self.winner)

    def get_compressed_state(self) -> CompressedGameState:
        return CompressedGameState(*map(tuple, self.session.run((self.ball_position, self.paddle_positions))))

    def get_pic(self, game_state: CompressedGameState = None, reshape=True, log=False):
        if game_state is not None:
            self.session.run(self.assign_draw_from_placeholder, feed_dict={
                self.draw_ball_position_placeholder: game_state.ball_position,
                self.draw_paddle_positions_placeholder: game_state.paddle_positions,
            })
        else:
            self.session.run(self.assign_draw_from_current)

        if log:
            result, summary = self.session.run((self.draw, self.picture_summary))
            # print('STEP', self.global_step)
            self.file_writer.add_summary(summary, self.global_step)
        else:
            result = self.session.run(self.draw)

        return result if not reshape else np.reshape(result, self.image_size_value)

    def train(self, episodes=500):
        self._new_episode()
        self._fill_initial_replay_memory()

        self.global_step = 0

        episode_losses = []

        for episode in tqdm(range(1, episodes + 1), desc='Episodes'):
            before = self.global_step
            self._new_episode()
            play_episode = self.play_episode(update=True, collect_memories=True, collect_pictures=episode % 50 == 0)
            next(play_episode)

            losses = []

            try:
                while True:
                    # print(self._choose_opponent_action(self.history))
                    opponent_action = self._action_to_inputs(self._choose_opponent_action(self.history))
                    losses.append(play_episode.send(opponent_action))
            except StopIteration:
                pass

            print(f'Finished episode {episode}, step {self.global_step}, took {self.global_step - before}')

            # episode_losses.append(losses)

        self.saver.save(self.session, 'models/model.ckpt')

        return episode_losses

    def play_episode(self, update=True, collect_memories=True, collect_pictures=False):
        self._new_episode()

        if collect_pictures:
            self.pictures = []

        winner = Players.NO_PLAYER
        action = 4
        loss = None

        # frame_timer = PerformaceTimer(50)
        # update_timer = PerformaceTimer(50)
        while not winner:
            self.global_step += 1

            # with frame_timer:
            winner = self._repeat_action(self._do_step((yield loss), not update and not collect_pictures))
            # print('doing frame took', frame_timer.average_time, 'seconds on average')

            self._update_memory_and_pictures(action, winner, collect_memories, collect_pictures)

            if update:
                # with update_timer:
                loss = self._update_networks()
                # print('updating took', update_timer.average_time, 'seconds on average')

    def _repeat_action(self, winner):
        for _ in range(self.action_repeat):
            if winner:
                break
            winner = self.advance()
        return winner

    def _do_step(self, opponent_inputs, greedy=False):
        if greedy or (np.random.rand() > next(self.epsilons)):
            action = self._choose_greedy_action(self.history)
        else:
            action = self._choose_random_action()

        inputs = (
            self._action_to_inputs(action),
            opponent_inputs,
        )

        return self.advance(inputs)

    def _update_networks(self):
        loss = None

        if next(self.online_update_counter):
            # print('DOING: online update')
            loss = self._gradient_step()

        if next(self.target_update_counter):
            print('DOING: target update')
            self.session.run(self.update_target_network)

        if next(self.lagged_update_counter):
            print('DOING: lagged update')
            self.session.run(self.update_lagged_network)

        return loss

    def _update_memory_and_pictures(self, action, winner, update_memory, take_picture):
        current_pic = self.get_pic(log=take_picture)
        self._append_history(current_pic)

        # if take_picture:
        #    self.pictures.append(np.reshape(current_pic, (*reversed(self.image_size_value), 1)))

        state = self.get_compressed_state()

        if update_memory:
            memory_t = tuple(self.running_memory)
            self.running_memory.append(state)
            memory_t_plus_one = tuple(self.running_memory)

            self.replay_memory.append(Memory(
                memory_t,
                action,
                -winner,
                memory_t_plus_one
            ))

    def _fill_initial_running_memory(self):
        self.running_memory.extend([self.get_compressed_state()] * 4)

    def _append_history(self, frames):
        self.history[:, :, :-1] = self.history[:, :, 1:]
        self.history[:, :, -1] = frames

    def _gradient_step(self, log=True):
        indices = np.random.choice(len(self.replay_memory), self.minibatch_size, replace=False)
        minibatch = [self.replay_memory[index] for index in indices]
        current_frames, actions, rewards, next_frames = zip(*minibatch)

        feed_dict = {
            self.online_frames: list(map(self._frames_from_experience, current_frames)),
            self.next_experienced_frames: list(map(self._frames_from_experience, next_frames)),
            self.experienced_action: list(actions),
            self.experienced_reward: list(rewards),
        }

        if not log:
            loss, _ = self.session.run((self.loss, self.optimizer_step), feed_dict=feed_dict)
        else:
            loss, loss_summary, _ = self.session.run((self.loss, self.loss_summary, self.optimizer_step),
                                                     feed_dict=feed_dict)
            self.file_writer.add_summary(loss_summary, self.global_step)

        return loss

    def _frames_from_experience(self, experience):
        return np.array(list(map(self.get_pic, experience))).swapaxes(0, 1).swapaxes(1, 2)

    def _fill_initial_replay_memory(self):
        for _ in self.replay_start_counter:
            actions = self._choose_random_action(2)
            inputs = self._action_to_inputs(actions)

            winner = self.advance(inputs)

            self._update_memory_and_pictures(actions[0], winner, True, False)

            if winner:
                self._new_episode()

    def _action_to_inputs(self, action):
        return np.array((
            (1, 0),
            (0, 1),
            (0, 0),
            (1, 1),
        ))[action]

    def _choose_random_action(self, size=()):
        return np.random.randint(0, 4, size)

    def _choose_opponent_action(self, frames):
        flipped_frames = np.flip(frames, 1)
        res = self.session.run(self.lagged_network, feed_dict={
            self.lagged_frames: (flipped_frames,),
        })
        # print(res)
        return np.argmax(res)

    def _choose_greedy_action(self, frames):
        values, summary = self.session.run((self.online_network, self.online_summary), feed_dict={
            self.online_frames: (frames,),
        })
        self.file_writer.add_summary(summary, self.global_step)
        return np.argmax(values)

    def _make_ball(self):
        self.ball_velocity_placeholder = tf.placeholder(tf.float32, 2, 'velocity_placeholder')
        self.ball_position_placeholder = tf.placeholder(tf.float32, 2, 'ball_position_placeholder')

        self.ball_velocity = tf.get_variable('ball_velocity', 2, tf.float32, tf.zeros_initializer)
        self.ball_position = tf.get_variable('ball_position', 2, tf.float32, tf.zeros_initializer)

        self.set_ball_velocity = tf.assign(self.ball_velocity, self.ball_velocity_placeholder)
        self.set_ball_position = tf.assign(self.ball_position, self.ball_position_placeholder)

    def _make_paddles(self):
        self.inputs_placeholder = tf.placeholder(tf.float32, (2, 2), 'inputs_placeholder')
        self.paddle_velocities_placeholder = tf.placeholder(tf.float32, 2, 'paddle_velocities_placeholder')
        self.paddle_positions_placeholder = tf.placeholder(tf.float32, 2, 'paddle_positions_placeholder')

        self.inputs = tf.get_variable('player_inputs', (2, 2), tf.float32, tf.zeros_initializer)
        self.paddle_velocities = tf.get_variable('paddle_velocities', 2, tf.float32, tf.zeros_initializer)
        self.paddle_positions = tf.get_variable('paddle_positions', 2, tf.float32, tf.zeros_initializer)

        self.set_inputs = tf.assign(self.inputs, self.inputs_placeholder)
        self.set_paddle_velocities = tf.assign(self.paddle_velocities, self.paddle_velocities_placeholder)
        self.set_paddle_positions = tf.assign(self.paddle_positions, self.paddle_positions_placeholder)

    def _update_paddles(self):
        new_velocities = self.paddle_velocities + tf.reshape(self.inputs @ self.force_factor, (-1,))
        new_velocities -= self.friction_factor * new_velocities

        new_positions = self.paddle_positions + new_velocities

        new_positions, parity, pu = self._rectify(new_positions, self.paddle_sizes)

        bumps = np.abs(pu)

        new_velocities = self._conditional_negative(parity, new_velocities)
        new_velocities *= self.bump_factor ** bumps

        assign_velocities = tf.assign(self.paddle_velocities, new_velocities)
        assign_positions = tf.assign(self.paddle_positions, new_positions)

        self.step_paddles = tf.group(assign_velocities, assign_positions)

    def _update_ball(self):
        new_position = self.ball_position + self.ball_velocity
        new_position, *_ = self._rectify(new_position, self.ball_size)

        new_velocity = self.ball_velocity

        self._will_hit_x()
        self._will_hit_pad()

        paddle_boosts = self.paddle_velocities * self.paddle_factor * tf.constant(((0,), (1,)), tf.float32)
        new_velocity = self._conditional_add(self.has_hit, new_velocity, self._select(paddle_boosts, self.will_hit))
        new_velocity = tf.clip_by_value(new_velocity, -1, 1)

        new_position = self.ball_position + new_velocity
        new_position, parity, _ = self._rectify(new_position, self.ball_size)

        new_velocity = self._conditional_negative(parity, new_velocity)

        assign_velocity = tf.assign(self.ball_velocity, new_velocity)
        assign_position = tf.assign(self.ball_position, new_position)

        self.step_ball = tf.group(assign_velocity, assign_position)

    def _draw(self):
        self.draw_ball_position = tf.get_variable('draw_ball_position', initializer=self.ball_position)
        self.draw_paddle_positions = tf.get_variable('draw_paddle_positions', initializer=self.paddle_positions)

        self.draw_ball_position_placeholder = tf.placeholder(tf.float32, 2, 'draw_ball_position_placeholder')
        self.draw_paddle_positions_placeholder = tf.placeholder(tf.float32, 2, 'draw_paddle_positions_placeholder')

        assign_ball_position_from_placeholder = tf.assign(self.draw_ball_position, self.draw_ball_position_placeholder)
        assign_paddle_positions_from_placeholder = tf.assign(self.draw_paddle_positions,
                                                             self.draw_paddle_positions_placeholder)
        self.assign_draw_from_placeholder = tf.group(assign_ball_position_from_placeholder,
                                                     assign_paddle_positions_from_placeholder)

        assign_ball_position_from_current = tf.assign(self.draw_ball_position, self.ball_position)
        assign_paddle_positions_from_current = tf.assign(self.draw_paddle_positions, self.paddle_positions)
        self.assign_draw_from_current = tf.group(assign_ball_position_from_current,
                                                 assign_paddle_positions_from_current)

        pixel_ball_size = self._scale(self.ball_size)
        pixel_ball_top_left = self._pixel(self.draw_ball_position) - pixel_ball_size

        ball_range_x, ball_range_y = self._make_ranges(pixel_ball_size[0] * 2, pixel_ball_size[1] * 2)

        ball_indices = self._rectangle_indices(
            left=pixel_ball_top_left[0],
            top=pixel_ball_top_left[1],
            range_x=ball_range_x,
            range_y=ball_range_y,
        )

        paddle_pixel_positions = self._pixel_y(self.draw_paddle_positions)
        pixel_paddle_sizes = self._scale_y(self.paddle_sizes)

        paddle_tops = paddle_pixel_positions - pixel_paddle_sizes

        left_paddle_left = self.margins[0] - self.paddle_pixel_widths[0]
        right_paddle_left = self.image_size[0] - self.margins[0]

        left_paddle_range_x, left_paddle_range_y = self._make_ranges(self.paddle_pixel_widths[0],
                                                                     pixel_paddle_sizes[0] * 2)
        right_paddle_range_x, right_paddle_range_y = self._make_ranges(self.paddle_pixel_widths[1],
                                                                       pixel_paddle_sizes[1] * 2)

        left_paddle_indices = self._rectangle_indices(
            left=left_paddle_left,
            top=paddle_tops[0],
            range_x=left_paddle_range_x,
            range_y=left_paddle_range_y,
        )

        right_paddle_indices = self._rectangle_indices(
            left=right_paddle_left,
            top=paddle_tops[1],
            range_x=right_paddle_range_x,
            range_y=right_paddle_range_y,
        )

        with tf.control_dependencies([self.image.initializer]):
            self.draw = tf.scatter_update(self.image, tf.cast(tf.concat((
                ball_indices,
                left_paddle_indices,
                right_paddle_indices
            ), axis=0), tf.int64), 1)

        self.picture_summary = tf.summary.image('pictures',
                                                tf.reshape(self.draw, (1, *reversed(self.image_size_value), 1)))

    def _rectify(self, vector, margin):
        vector = self._to_margin_coords(vector, margin)

        pu_coordinates = tf.cast(tf.cast(vector, tf.int64), tf.float32)
        direction = tf.sign(vector)

        parity = pu_coordinates % 2
        in_range = vector % (tf.sign(direction * 2 + 1))
        rectified = self._conditional(parity, direction - in_range, in_range)
        rectified = self._from_margin_coords(rectified, margin)

        return rectified, parity, pu_coordinates

    def _will_hit_x(self):
        position_ball_x = self.ball_position[0]
        position_ball_y = self.ball_position[1]
        velocity_ball_x = self.ball_velocity[0]
        velocity_ball_y = self.ball_velocity[1]

        factor = (self.border - position_ball_x) / velocity_ball_x
        self.will_hit = self._is_zero(tf.floor(factor))

        relevant_factor = self._select(factor, self.will_hit)
        self.hit_at = position_ball_y + relevant_factor * velocity_ball_y

    def _will_hit_pad(self):
        pad_position = self._select(self.paddle_positions, self.will_hit),
        pad_size = self._select(self.paddle_sizes, self.will_hit)

        ball_bot_top = self.hit_at + tf.constant((1, -1), tf.float32) * self.ball_size[1]
        pad_top_bot = pad_position + tf.constant((-1, 1), tf.float32) * pad_size
        self.has_hit = self._is_zero(tf.reduce_sum(tf.sign(ball_bot_top - pad_top_bot)))

    def _select(self, vector, selector):
        # return tf.tensordot(vector, selector, 1)
        # return tf.reduce_sum(vector * selector)
        if (len(vector.get_shape()) == 0):
            return vector * selector
        if (len(vector.get_shape()) == 1):
            return tf.reduce_sum(vector * selector)
        return tf.reshape((vector @ tf.reshape(selector, (-1, 1))), (-1,))

    def _scale_y(self, value):
        return tf.cast(tf.round(value / 2 * (self.image_size[1] - 2 * self.margins[1])), tf.int64)

    def _scale(self, value):
        return tf.cast(tf.round(value / 2 * (self.image_size - 2 * self.margins)), tf.int64)

    def _pixel_y(self, value):
        return tf.cast(
            tf.round(self.margins[1] + (value + 1) * (self.image_size[1] - 2 * self.margins[1]) / 2), tf.int64
        )

    def _pixel(self, vector):
        return tf.cast(tf.round(self.margins + (vector + 1) * (self.image_size - 2 * self.margins) / 2), tf.int64)

    def _from_margin_coords(self, vector, margin):
        return vector * (1 - margin)

    def _to_margin_coords(self, vector, margin):
        return vector / (1 - margin)

    def _non_zero(self, vector):
        return tf.sign(tf.abs(vector))

    def _is_zero(self, vector):
        return 1 - self._non_zero(vector)

    def _conditional(self, cond, then_val, else_val):
        return cond * then_val + (1 - cond) * else_val

    def _conditional_negative(self, cond, value):
        return value - 2 * cond * value

    def _conditional_add(self, cond, base_value, amount):
        return base_value + cond * amount

    def _rectangle_indices(self, left: int, top: int, range_x, range_y):
        range_x = tf.cast(range_x + left, tf.float32)
        range_y = tf.cast(range_y + top, tf.float32) * self.image_size[0]

        return tf.reshape(range_x + range_y, (-1,))

    def _make_ranges(self, size_x, size_y):
        range_x = tf.reshape(tf.range(size_x), (1, -1))
        range_y = tf.reshape(tf.range(size_y), (-1, 1))
        return range_x, range_y

    def _make_q_networks(self):
        self.next_experienced_frames = tf.placeholder(tf.float32, (None, *self.history.shape))
        self.experienced_action = tf.placeholder(tf.float32, (None,))
        self.experienced_reward = tf.placeholder(tf.float32, (None,))

        def make(name, frames=None, reuse=False):
            if frames is None:
                frames = tf.placeholder(tf.float32, (None, *self.history.shape))

            with tf.variable_scope(name, reuse=reuse):
                activations = tf.layers.conv2d(frames, 32, (8, 8), 4, activation=tf.nn.relu)
                # activations = tf.layers.max_pooling2d(activations, 2, 2)
                activations = tf.layers.conv2d(activations, 64, (4, 4), 2, activation=tf.nn.relu)
                # activations = tf.layers.max_pooling2d(activations, 2, 2)
                activations = tf.layers.conv2d(activations, 64, (3, 3), 1, activation=tf.nn.relu)
                # activations = tf.layers.max_pooling2d(activations, 2, 2)

                activations = tf.layers.flatten(activations)

                activations = tf.layers.dense(activations, 512, activation=tf.nn.relu)

                values = tf.layers.dense(activations, 4)
                value_summary = tf.summary.histogram('Q-Values', values)

            variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, name)

            return frames, values, variables, value_summary

        self.online_frames, self.online_network, self.online_parameters, self.online_summary = make('OnlineNetwork')
        self.choice_frames, self.choice_network, _, _ = make('OnlineNetwork', self.next_experienced_frames, reuse=True)
        _, self.target_network, self.target_parameters, _ = make('TargetNetwork', self.next_experienced_frames)
        self.lagged_frames, self.lagged_network, self.lagged_parameters, _ = make('LaggedNetwork')

        self.update_target_network = tf.group(*[
            tf.assign(target_parameter, online_parameter)
            for online_parameter, target_parameter in zip(self.online_parameters, self.target_parameters)
        ])

        self.update_lagged_network = tf.group(*[
            tf.assign(lagged_parameter, online_parameter)
            for online_parameter, lagged_parameter in zip(self.online_parameters, self.lagged_parameters)
        ])

        self.chosen = tf.argmax(self.choice_network, axis=1)

        # self.targets = -self.experienced_reward + self.discount_factor * self.target_network[self.chosen]
        self.targets = -self.experienced_reward + self.discount_factor * self._index(self.target_network, self.chosen)
        self.loss = tf.losses.mean_squared_error(self.targets,
                                                 self._index(self.online_network, self.experienced_action))

        self.loss_summary = tf.summary.scalar('loss', self.loss)

        self.optimizer_step = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss)

    def _index(self, tensor, indices):
        idx = tf.cast(tf.range(0, limit=tf.shape(indices)[0], delta=1), tf.int64)
        idx = tf.stack([idx, tf.cast(indices, tf.int64)], axis=-1)
        return tf.gather_nd(tensor, idx)


if __name__ == '__main__':
    image_size = np.array((100, 50))
    ratio = image_size[1] / image_size[0]

    game = Pong(
        ball_size=0.05 * np.array((ratio, 1)),
        paddle_sizes=np.array((0.2, 0.2)),
        image_size=image_size.astype(np.int64),
        margins=np.array((10, 0), np.int64),
        paddle_pixel_widths=np.array((4, 4), np.int64),
        paddle_factor=0.1,
        force_factor=np.array(((-0.01,), (0.01,))),
        friction_factor=0.2,
        bump_factor=0.5,
    )

    game.initialize()

    losses = game.train()
    print(losses)
