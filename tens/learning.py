from typing import NamedTuple
import numpy as np


class CompressedGameState(NamedTuple):
    ball_position: tuple
    paddle_positions: tuple


def to_pic(compressed_game_state: CompressedGameState):
    pass
