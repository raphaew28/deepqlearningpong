import numpy as np
from PyQt5 import Qt, QtGui

from model import Players
from tens.game import simulate_tf


class GameWidget(Qt.QLabel):
    # newFrame = Qt.pyqtSignal(Qt.QImage)
    gameEnded = Qt.pyqtSignal(int)
    restart = Qt.pyqtSignal()

    def __init__(self):
        super().__init__()

        self.setFocusPolicy(Qt.Qt.StrongFocus)

        self.frameTimer = Qt.QTimer()
        self.frameTimer.setInterval(1000 / 60)
        self.frameTimer.timeout.connect(self.advanceFrameSlot)

        self.keysPressed = {}

    def start(self) -> None:
        self.frameTimer.start()

    def pause(self) -> None:
        self.frameTimer.stop()

    def newGame(
            self,
            initial_inputs_value,
            initial_ball_position_value,
            initial_ball_velocity_value,
            initial_paddle_positions_value,
            initial_paddle_velocities_value,
            ball_size,
            paddle_sizes,
            image_size,
            margins,
            paddle_pixel_widths,
            paddle_factor,
            force_factor,
            friction_factor,
            bump_factor,
    ) -> None:
        self.image_size = image_size
        self.winner = Players.NO_PLAYER

        self.runner = simulate_tf(
            initial_inputs_value,
            initial_ball_position_value,
            initial_ball_velocity_value,
            initial_paddle_positions_value,
            initial_paddle_velocities_value,
            ball_size,
            paddle_sizes,
            image_size,
            margins,
            paddle_pixel_widths,
            paddle_factor,
            force_factor,
            friction_factor,
            bump_factor,
        )
        next(self.runner)

    def advanceFrameSlot(self) -> None:
        inputs = np.array((
            (self.keysPressed.get(Qt.Qt.Key_W, False), self.keysPressed.get(Qt.Qt.Key_S, False)),
            (self.keysPressed.get(Qt.Qt.Key_Up, False), self.keysPressed.get(Qt.Qt.Key_Down, False)),
        ), np.float32)

        winner, image = self.runner.send(inputs)
        self.gameEnded.emit(self.winner)

        self.setPixmap(Qt.QPixmap.fromImage(Qt.QImage(
            np.uint8(image * 255),
            self.image_size[0],
            self.image_size[1],
            self.image_size[0],
            Qt.QImage.Format_Grayscale8
        )))

        # if self.winner:
        #     self.pause()
        #     self.gameEnded.emit(winner)
        #     self.runner.close()
        #     return
        self.winner = winner

    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        self.keysPressed[event.key()] = True

    def keyReleaseEvent(self, event: QtGui.QKeyEvent) -> None:
        if event.key() in self.keysPressed:
            self.keysPressed[event.key()] = False

        if event.key() == Qt.Qt.Key_Space:
            if self.frameTimer.isActive():
                self.pause()
            else:
                self.start()

        elif event.key() == Qt.Qt.Key_R:
            self.restart.emit()


class MainWin(Qt.QMainWindow):
    def __init__(self):
        super().__init__()

        self.initMenulbar()

        self.gameThread = Qt.QThread()
        self.gameWidget = GameWidget()

        self.gameWidget.moveToThread(self.gameThread)
        self.gameThread.start()

        self.newGame()

        self.setMinimumSize(1000, 500)
        self.setCentralWidget(self.gameWidget)

        self.gameWidget.gameEnded.connect(self.announceWinner)
        self.gameWidget.restart.connect(self.newGame)

    def announceWinner(self, winner: Players):
        names = {
            -1: 'Left Player Won',
            0: 'Game Ongoing',
            1: 'Right Player Won',
        }
        self.setWindowTitle(names[winner])

    def initMenulbar(self):
        menubar = self.menuBar()
        menubar.addAction('New game', self.newGame)

    def newGame(self):
        self.announceWinner(Players.NO_PLAYER)

        image_size = np.array((800, 400))
        ratio = image_size[1] / image_size[0]

        self.gameWidget.pause()
        self.gameWidget.newGame(
            initial_inputs_value=np.array(((0, 0), (0, 0))),
            initial_ball_position_value=np.array((0.8, 0)),
            initial_ball_velocity_value=np.array((0.01, 0)),
            initial_paddle_positions_value=np.array((0, 0.215)),
            initial_paddle_velocities_value=np.array((0, 0.01)),
            ball_size=0.05 * np.array((ratio, 1)),
            paddle_sizes=np.array((0.2, 0.2)),
            image_size=np.array((800, 400), np.int64),
            margins=np.array((20, 0), np.int64),
            paddle_pixel_widths=np.array((7, 7), np.int64),
            paddle_factor=0.1,
            force_factor=np.array(((-0.01,), (0.01,))),
            friction_factor=0.2,
            bump_factor=0.5,
        )

        self.gameWidget.start()


if __name__ == '__main__':
    app = Qt.QApplication([])

    mainWin = MainWin()
    mainWin.setWindowTitle('Pong Reinforcement Learning')
    mainWin.show()

    app.exec()
