from typing import Generator, Tuple, Any

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt

from model import Players
from tens.q_network import make_q_nets, train_episode


class print_tensors():
    def __init__(self, result, *inputs, **kwargs):
        self.result = result
        self.inputs = inputs
        self.kwargs = kwargs
        self.manager = tf.control_dependencies((tf.print(*inputs, **kwargs),))

    def __enter__(self):
        self.manager.__enter__()
        return tf.identity(self.result)

    def __exit__(self, *args):
        self.manager.__exit__(*args)


def save_tensor_image(tensor, image_size, name):
    plt.imsave(name, np.reshape(tensor, image_size))


def scale_y(value, image_size, margin):
    return tf.cast(tf.round(value / 2 * (image_size[1] - 2 * margin[1])), tf.int64)


def scale(value, image_size, margin):
    return tf.cast(tf.round(value / 2 * (image_size - 2 * margin)), tf.int64)


def pixel_y(value, image_size, margin):
    return tf.cast(tf.round(margin[1] + (value + 1) * (image_size[1] - 2 * margin[1]) / 2), tf.int64)


def pixel(vector, image_size, margin):
    return tf.cast(tf.round(margin + (vector + 1) * (image_size - 2 * margin) / 2), tf.int64)


def from_margin_coords(vector, margin):
    return vector * (1 - margin)


def to_margin_coords(vector, margin):
    return vector / (1 - margin)


def non_zero(vector):
    return tf.sign(tf.abs(vector))


def is_zero(vector):
    return 1 - non_zero(vector)


def conditional(cond, then_val, else_val):
    return cond * then_val + (1 - cond) * else_val


def conditional_negative(cond, value):
    return value - 2 * cond * value


def conditional_add(cond, base_value, amount):
    return base_value + cond * amount


def rectify(vector, margin):
    vector = to_margin_coords(vector, margin)

    pu_coordinates = tf.cast(tf.cast(vector, tf.int64), tf.float32)
    direction = tf.sign(vector)

    parity = pu_coordinates % 2
    in_range = vector % (tf.sign(direction * 2 + 1))
    rectified = conditional(parity, direction - in_range, in_range)
    rectified = from_margin_coords(rectified, margin)

    return rectified, parity, pu_coordinates


def rectangle_indices(left: int, top: int, range_x, range_y, image_size):
    range_x = range_x + left
    range_y = (range_y + top) * image_size[0]

    return tf.reshape(range_x + range_y, (-1,))


def make_ranges(size_x, size_y):
    range_x = tf.reshape(tf.range(size_x, dtype=tf.int64), (1, -1))
    range_y = tf.reshape(tf.range(size_y, dtype=tf.int64), (-1, 1))
    return range_x, range_y


def draw_image(image_size, *indices):
    image = tf.get_variable('image', np.prod(image_size), initializer=tf.zeros_initializer)
    with tf.control_dependencies([image.initializer]):
        return tf.scatter_update(image, tf.concat(indices, axis=0), 1)


def make_ball(image_size, margin, ball_size):
    initial_ball_velocity = tf.placeholder(tf.float32, 2, 'initial_ball_velocity_placeholder')
    initial_ball_position = tf.placeholder(tf.float32, 2, 'initial_ball_position')

    ball_velocity = tf.get_variable('ball_velocity', initializer=initial_ball_velocity, dtype=tf.float32)
    ball_pos = tf.get_variable('ball_position', initializer=initial_ball_position, dtype=tf.float32)

    pixel_ball_size = scale(ball_size, image_size, margin)
    pixel_ball_top_left = pixel(ball_pos, image_size, margin) - pixel_ball_size

    ball_range_x, ball_range_y = make_ranges(pixel_ball_size[0] * 2, pixel_ball_size[1] * 2)

    ball_indices = rectangle_indices(
        left=pixel_ball_top_left[0],
        top=pixel_ball_top_left[1],
        range_x=ball_range_x,
        range_y=ball_range_y,
        image_size=image_size,
    )

    return (
        initial_ball_velocity,
        initial_ball_position,
        ball_velocity,
        ball_pos,
        ball_indices,
    )


def make_paddles(image_size, margin, paddle_sizes, paddle_widths):
    inputs_placeholder = tf.placeholder(tf.float32, (2, 2), 'player_inputs')
    initial_paddle_velocities = tf.placeholder(tf.float32, 2, 'initial_paddle_velocities')
    initial_paddle_positions = tf.placeholder(tf.float32, 2, 'initial_paddle_positions')

    inputs = tf.get_variable('player_inputs', initializer=inputs_placeholder, dtype=tf.float32)
    paddle_velocities = tf.get_variable('paddle_velocities', initializer=initial_paddle_velocities, dtype=tf.float32)
    paddle_positions = tf.get_variable('paddle_positions', initializer=initial_paddle_positions, dtype=tf.float32)

    paddle_pixel_positions = pixel_y(paddle_positions, image_size, margin)
    pixel_paddle_sizes = scale_y(paddle_sizes, image_size, margin)

    paddle_tops = paddle_pixel_positions - pixel_paddle_sizes

    left_paddle_left = margin[0] - paddle_widths[0]
    right_paddle_left = image_size[0] - margin[0]

    left_paddle_range_x, left_paddle_range_y = make_ranges(paddle_widths[0], pixel_paddle_sizes[0] * 2)
    right_paddle_range_x, right_paddle_range_y = make_ranges(paddle_widths[1], pixel_paddle_sizes[1] * 2)

    left_paddle_indices = rectangle_indices(
        left=left_paddle_left,
        top=paddle_tops[0],
        range_x=left_paddle_range_x,
        range_y=left_paddle_range_y,
        image_size=image_size,
    )

    right_paddle_indices = rectangle_indices(
        left=right_paddle_left,
        top=paddle_tops[1],
        range_x=right_paddle_range_x,
        range_y=right_paddle_range_y,
        image_size=image_size,
    )

    set_inputs = tf.assign(inputs, inputs_placeholder)

    return (
        inputs_placeholder,
        initial_paddle_velocities,
        initial_paddle_positions,
        inputs,
        paddle_velocities,
        paddle_positions,
        left_paddle_indices,
        right_paddle_indices,
        set_inputs,
    )


def update_paddles(
        inputs,
        paddle_velocities,
        paddle_positions,
        paddle_sizes,
        force_factor,
        friction_factor,
        bump_factor,
):
    new_velocities = paddle_velocities + tf.reshape(inputs @ force_factor, (-1,))
    new_velocities -= friction_factor * new_velocities

    new_positions = paddle_positions + new_velocities

    new_positions, parity, pu = rectify(new_positions, paddle_sizes)

    bumps = np.abs(pu)

    new_velocities = conditional_negative(parity, new_velocities)
    new_velocities *= bump_factor ** bumps

    assign_velocities = tf.assign(paddle_velocities, new_velocities)
    assign_positions = tf.assign(paddle_positions, new_positions)

    return tf.group(assign_velocities, assign_positions)


def will_hit_x(position_ball, velocity_ball, ball_size):
    position_ball_x = position_ball[0]
    position_ball_y = position_ball[1]
    velocity_ball_x = velocity_ball[0]
    velocity_vall_y = velocity_ball[1]

    factor = (tf.constant((ball_size[0] - 1, 1 - ball_size[0])) - position_ball_x) / velocity_ball_x
    will_hit = is_zero(tf.floor(factor))

    relevant_factor = select(factor, will_hit)
    hit_at = position_ball_y + relevant_factor * velocity_vall_y

    return will_hit, hit_at


def will_hit_pad(position_ball_y, size_ball, size_pad, position_pad):
    ball_bot_top = position_ball_y + tf.constant((1, -1), tf.float32) * size_ball[1]
    pad_top_bot = position_pad + tf.constant((-1, 1), tf.float32) * size_pad

    has_hit = is_zero(tf.reduce_sum(tf.sign(ball_bot_top - pad_top_bot)))

    return has_hit


def select(vector, selector):
    # return tf.tensordot(vector, selector, 1)
    # return tf.reduce_sum(vector * selector)
    if (len(vector.get_shape()) == 0):
        return vector * selector
    if (len(vector.get_shape()) == 1):
        return tf.reduce_sum(vector * selector)
    return tf.reshape((vector @ tf.reshape(selector, (-1, 1))), (-1,))


def update_ball(
        ball_position,
        ball_velocity,
        ball_size,
        ball_size_constant,
        players_size,
        players_velocity,
        player_position,
        paddle_factor
):
    new_position = ball_position + ball_velocity
    new_position, *_ = rectify(new_position, ball_size)

    new_velocity = ball_velocity

    will_hit, hit_at = will_hit_x(new_position, new_velocity, ball_size)

    has_hit = will_hit_pad(
        hit_at,
        ball_size_constant,
        select(players_size, will_hit),
        select(player_position, will_hit)
    )

    paddle_boosts = players_velocity * paddle_factor * tf.constant(((0,), (1,)), tf.float32)
    new_velocity = conditional_add(has_hit, new_velocity, select(paddle_boosts, will_hit))
    new_velocity = tf.clip_by_value(new_velocity, -1, 1)

    new_position = ball_position + new_velocity
    new_position, parity, _ = rectify(new_position, ball_size)

    new_velocity = conditional_negative(parity, new_velocity)

    assign_velocity = tf.assign(ball_velocity, new_velocity)
    assign_position = tf.assign(ball_position, new_position)

    with tf.control_dependencies([assign_velocity, assign_position]):
        winner = select((1 - has_hit) * tf.constant((1, -1), tf.float32), will_hit)
        return winner


def make_graph(
        image_size,
        margin,
        paddle_widths,
        ball_size,
        paddle_sizes,
        paddle_factor,
        force_factor,
        friction_factor,
        bump_factor,
):
    graph = tf.Graph()

    with graph.as_default():
        (
            initial_ball_velocity,
            initial_ball_position,
            ball_velocity,
            ball_position,
            ball_indices
        ) = make_ball(image_size, margin, ball_size)

        (
            inputs_placeholder,
            initial_paddle_velocities,
            initial_paddle_positions,
            inputs,
            paddle_velocities,
            paddle_positions,
            left_paddle_indices,
            right_paddle_indices,
            set_inputs,
        ) = make_paddles(image_size, margin, paddle_sizes, paddle_widths)

        draw = draw_image(image_size, ball_indices, left_paddle_indices, right_paddle_indices)

        move_paddles = update_paddles(
            inputs,
            paddle_velocities,
            paddle_positions,
            paddle_sizes,
            force_factor,
            friction_factor,
            bump_factor,
        )

        with tf.control_dependencies([move_paddles]):
            winner = update_ball(
                ball_position,
                ball_velocity,
                ball_size,
                tf.constant(ball_size, tf.float32),
                tf.constant(paddle_sizes, tf.float32),
                players_velocity=paddle_velocities,
                player_position=paddle_positions,
                paddle_factor=paddle_factor
            )

        initializer = tf.global_variables_initializer()

        return (
            graph,
            initializer,
            inputs_placeholder,
            initial_ball_velocity,
            initial_ball_position,
            initial_paddle_velocities,
            initial_paddle_positions,
            inputs,
            draw,
            winner,
            set_inputs,
        )


def simulate_tf(
        initial_inputs_value,
        initial_ball_position_value,
        initial_ball_velocity_value,
        initial_paddle_positions_value,
        initial_paddle_velocities_value,
        ball_size,
        paddle_sizes,
        image_size,
        margins,
        paddle_pixel_widths,
        paddle_factor,
        force_factor,
        friction_factor,
        bump_factor,
) -> Generator[Tuple[Players, np.ndarray], Any, None]:
    (
        graph,
        initializer,
        inputs_placeholder,
        initial_ball_velocity,
        initial_ball_position,
        initial_paddle_velocities,
        initial_paddle_positions,
        inputs,
        draw,
        winner,
        set_inputs,
    ) = make_graph(
        image_size,
        margins,
        paddle_pixel_widths,
        ball_size,
        paddle_sizes,
        paddle_factor,
        force_factor,
        friction_factor,
        bump_factor,
    )

    with tf.Session(graph=graph) as session:
        session.run(initializer, feed_dict={
            inputs_placeholder: initial_inputs_value,
            initial_ball_velocity: initial_ball_velocity_value,
            initial_ball_position: initial_ball_position_value,
            initial_paddle_velocities: initial_paddle_velocities_value,
            initial_paddle_positions: initial_paddle_positions_value,
        })

        state, online_network, target_network, lagged_network, update_target_network, update_lagged_network = make_q_nets(
            image_size, 4,
        )
        train_episode(
            session.run, state, online_network, winner, draw, 4, 0, 0, inputs_placeholder,
        )

        # while True:
        #     session.run(set_inputs, feed_dict={
        #         inputs_placeholder: (yield session.run([winner, draw])),
        #     })
