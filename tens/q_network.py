import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt

from model import Players


def make_dual_net(make_net, online_name, lagged_name):
    online_network = make_net(online_name)
    lagged_network = make_net(lagged_name)

    online_parameters = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, online_name)
    lagged_parameters = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, lagged_name)

    copy_network_params_assignments = tf.group(*[
        tf.assign(target_parameter, online_parameter)
        for online_parameter, target_parameter in zip(online_parameters, lagged_parameters)
    ])

    return online_network, lagged_network, copy_network_params_assignments


def make_q_nets(image_size, history_length):
    state = tf.placeholder(tf.float32, (None, *image_size, history_length))

    def make(name):
        with tf.variable_scope(name):
            activations = tf.layers.conv2d(state, 32, (8, 8), 4, activation=tf.nn.relu)
            activations = tf.layers.conv2d(activations, 64, (4, 4), 2, activation=tf.nn.relu)
            activations = tf.layers.conv2d(activations, 64, (3, 3), 1, activation=tf.nn.relu)

            activations = tf.layers.dense(activations, 512, activation=tf.nn.relu)
            values = tf.layers.dense(activations, 2, activation=tf.nn.relu)

        variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, name)

        return values, variables

    online_network, online_parameters = make('CurrentOnlineNetwork')
    target_network, target_parameters = make('CurrentTargetNetwork')
    lagged_network, lagged_parameters = make('LaggedNetwork')

    update_target_network = tf.group(*[
        tf.assign(target_parameter, online_parameter)
        for online_parameter, target_parameter in zip(online_parameters, target_parameters)
    ])

    update_lagged_network = tf.group(*[
        tf.assign(lagged_parameter, online_parameter)
        for online_parameter, lagged_parameter in zip(online_parameters, lagged_parameters)
    ])

    return state, online_network, target_network, lagged_network, update_target_network, update_lagged_network


def do_action(session: tf.Session, inputs, inputs_placeholder, winner, draw, history_length, player):
    stack_winner = Players.NO_PLAYER
    frames = []
    for frame in range(history_length):
        frame_winner = session.run(winner, feed_dict={
            inputs_placeholder: inputs
        })
        frame = session.run(draw)

        if not stack_winner:
            stack_winner = frame_winner

        frames.append(frame)

    return frames, stack_winner == player


def select_actions(run, frames, epsilon, online_network, lagged_network, image_placeholder):
    flipped_frames = [np.fliplr(frame) for frame in frames]

    if (np.random.rand() < epsilon):
        return np.random.randint(0, 4)
    else:
        return np.argmax(run(online_network, feed_dict={
            image_placeholder: images
        }))


def train_episode(
        run,
        image_placeholder,
        online_network,
        target_network,
        lagged_network,
        winner,
        draw,
        history_length,
        player,
        epsilon,
        inputs_placeholder,
        initial_state=None,
        experience=None,
):
    if experience is None:
        experience = []

    episode_winner = Players.NO_PLAYER
    frames = [run(draw)] * 4 if initial_state is None else initial_state

    pic = np.reshape(frames[0], (400, 800))
    plt.imsave('frames/foobar.png', np.fliplr(pic))

    input_code = inputs = np.array((
        (1, 0),
        (0, 1),
        (0, 0),
        (1, 1),
    ))

    while not episode_winner:
        left_player_action = select_actions(run, frames, epsilon, online_network, image_placeholder)
        right_player_action = select_action(run, flipped_frames, 0, lagged_network, image_placeholder)

        frames, episode_winner = do_action(session, inputs, inputs_placeholder, winner, draw, history_length, player)


def training_step(session: tf.Session, winner, draw, online_network, target_network, copy_network, history, player):
    stack_winner = Players.NO_PLAYER
    for winner, image in history:
        if not stack_winner:
            stack_winner = winner
    reward = stack_winner == player
