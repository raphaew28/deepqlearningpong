import numpy as np
from PyQt5 import Qt, QtGui

from display import makePicture, DisplayConfig
from game import simulate, Inputs
from model import GameState, Ball, Players, Player


class GameWidget(Qt.QLabel):
    # newFrame = Qt.pyqtSignal(Qt.QImage)
    gameEnded = Qt.pyqtSignal(int)
    restart = Qt.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.displayConfig = DisplayConfig()

        self.setFocusPolicy(Qt.Qt.StrongFocus)

        self.frameTimer = Qt.QTimer()
        self.frameTimer.setInterval(16)
        self.frameTimer.timeout.connect(self.advanceFrameSlot)

        self.keysPressed = {}

    def start(self) -> None:
        self.frameTimer.start()

    def pause(self) -> None:
        self.frameTimer.stop()

    def updateGameState(self, gameState: GameState) -> None:
        self.gameState = gameState
        self.runner = simulate(gameState)
        next(self.runner)

    def updateDisplayConfig(self, displayConfig: DisplayConfig) -> None:
        self.displayConfig = displayConfig

    def advanceFrameSlot(self) -> None:
        inputs = Inputs(
            left_player=np.array((
                self.keysPressed.get(Qt.Qt.Key_W, False),
                self.keysPressed.get(Qt.Qt.Key_S, False),
            )),
            right_player=np.array((
                self.keysPressed.get(Qt.Qt.Key_Up, False),
                self.keysPressed.get(Qt.Qt.Key_Down, False),
            )),
        )

        try:
            self.gameState = self.runner.send((self.gameState, inputs))
        except StopIteration:
            self.pause()
            self.gameEnded.emit(self.gameState.winner)
            return

        picture = makePicture(self.gameState, self.displayConfig)

        # self.newFrame.emit(picture)
        self.setPixmap(Qt.QPixmap.fromImage(picture))
        self.update()

    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        self.keysPressed[event.key()] = True

    def keyReleaseEvent(self, event: QtGui.QKeyEvent) -> None:
        if event.key() in self.keysPressed:
            self.keysPressed[event.key()] = False

        if event.key() == Qt.Qt.Key_Space:
            if self.frameTimer.isActive():
                self.pause()
            else:
                self.start()

        elif event.key() == Qt.Qt.Key_R:
            self.restart.emit()


class MainWin(Qt.QMainWindow):
    def __init__(self):
        super().__init__()

        self.initMenulbar()

        self.gameThread = Qt.QThread()
        self.gameWidget = GameWidget()

        self.gameWidget.moveToThread(self.gameThread)
        self.gameThread.start()

        self.newGame()

        self.setMinimumSize(1000, 500)
        self.setCentralWidget(self.gameWidget)

        self.gameWidget.gameEnded.connect(self.announceWinner)
        self.gameWidget.restart.connect(self.newGame)

    def announceWinner(self, winner: Players):
        names = {
            -1: 'Left Player Won',
            0: 'Game Ongoing',
            1: 'Right Player Won',
        }
        self.setWindowTitle(names[winner])

    def initMenulbar(self):
        menubar = self.menuBar()
        menubar.addAction('New game', self.newGame)

    def newGame(self):
        self.announceWinner(Players.NO_PLAYER)

        self.gameWidget.pause()

        self.gameState = GameState(
            playerLeft=Player(
                0, 0, 0.5
            ),
            ball=Ball(
                velocity=np.array((
                    0.01,
                    0.005,
                )),
                radius=0.02
            ),
        )
        self.gameWidget.updateGameState(self.gameState)

        self.gameWidget.start()


if __name__ == '__main__':
    app = Qt.QApplication([])

    mainWin = MainWin()
    mainWin.setWindowTitle('Pong Reinforcement Learning')
    mainWin.show()

    app.exec()
