from typing import NamedTuple

import numpy as np
from PIL import Image, ImageDraw, ImageQt

from model import GameState, Vector2dFloat, Vector2dInt, Vector1dInt, Vector1dFloat

ColorType = str


class DisplayConfig(NamedTuple):
    size: Vector2dInt = np.array((800, 400), int)
    background_color: ColorType = 'black'
    left_paddle_color: ColorType = 'white'
    right_paddle_color: ColorType = 'white'
    ball_color: ColorType = 'white'
    paddle_width: Vector1dInt = 5
    margin: Vector1dInt = 20


def pixel_distance_x(vector: Vector1dFloat, size: Vector2dInt) -> Vector1dInt:
    return int((vector + 1) * size[0] / 2)


def pixel_distance_y(vector: Vector1dFloat, size: Vector2dFloat) -> Vector1dInt:
    return int((vector + 1) * size[1] / 2)


def pixel_distance(vector: Vector2dFloat, size: Vector2dInt) -> Vector2dInt:
    return ((vector + 1) * size / 2).astype(np.int)


def scale_to_pixel_x(value: Vector1dFloat, size: Vector2dInt) -> Vector1dInt:
    return value * size[0]


def scale_to_pixel_y(value: Vector1dFloat, size: Vector2dFloat) -> Vector1dInt:
    return value * size[1]


def scale_to_pixel(vector: Vector2dFloat, size: Vector2dInt) -> Vector2dInt:
    return vector * size


def makePicture(game_state: GameState, config: DisplayConfig = DisplayConfig(), filename: str = None):
    width, height = config.size
    width += 2 * config.margin

    image = Image.new('1', (width, height), 'black')
    draw = ImageDraw.Draw(image)

    def pixel_x(vector: Vector1dFloat) -> Vector1dInt:
        return config.margin + pixel_distance_x(vector, config.size)

    def pixel_y(vector: Vector1dFloat) -> Vector1dInt:
        return pixel_distance_y(vector, config.size)

    def pixel(vector: Vector2dFloat) -> Vector2dInt:
        raw_distance = pixel_distance(vector, config.size)
        raw_distance[0] += config.margin
        return raw_distance

    def scale_x(value: Vector1dFloat) -> Vector1dInt:
        return scale_to_pixel_x(value, config.size)

    def scale_y(value: Vector1dFloat) -> Vector1dInt:
        return scale_to_pixel_y(value, config.size) / 2

    def scale(vector: Vector2dFloat) -> Vector2dInt:
        return scale_to_pixel(vector, config.size)

    drawBall(
        draw,
        config.ball_color,
        pixel(game_state.ball.position),
        scale_y(game_state.ball.radius),
    )

    drawPaddle(
        draw,
        np.array((config.margin, pixel_y(game_state.playerLeft.position)), np.int),
        np.array((config.paddle_width, scale_y(game_state.playerLeft.size)), np.int),
        config.left_paddle_color,
        left_player=True,
    )

    drawPaddle(
        draw,
        np.array((width - config.margin, pixel_y(game_state.playerRight.position)), np.int),
        np.array((config.paddle_width, scale_y(game_state.playerRight.size)), np.int),
        config.right_paddle_color,
        left_player=False,
    )

    del draw

    if filename is None:
        return ImageQt.ImageQt(image)

    image.save(filename)


def drawBall(draw: ImageDraw, color: ColorType, position: Vector2dInt, radius: Vector1dInt):
    top_left = position - (radius, radius)
    bottom_right = position + (radius, radius)

    draw.rectangle((*top_left, *bottom_right), fill=color)


def drawPaddle(draw: ImageDraw, position: Vector2dInt, size: Vector2dInt, color: ColorType, left_player: bool):
    top_left = position + size * (np.array((1 if left_player else 0, 1)))
    bottom_right = position - size * (np.array((0 if left_player else 1, 1)))

    draw.rectangle((*top_left, *bottom_right), fill=color)
